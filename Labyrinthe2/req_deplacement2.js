'use strict'

require('remedial')
const fs = require("fs");
const afficher_laby2 =  require("./fct_afficher_laby2");
const trait = require("../req_afficher_fini_jouer");

const req_deplacement2 = function (req, res, query) {
	let contenu;
	let marqueurs;
	let page;
	let donnees;
	let map;

	//recuperation des données
    contenu = fs.readFileSync("labyrinthe2.json", "utf-8");
    donnees = JSON.parse(contenu);
	contenu = fs.readFileSync("map2.json", "utf-8");
	map = JSON.parse(contenu);
	
	//algo
	donnees.perso.y = Number(query.y);
	donnees.perso.x = Number(query.x);
	donnees.pm = Number(query.pm);	

	if (donnees.perso.y === 0 && donnees.perso.x === 0) {
		trait(req, res, query);
	}

	//stringify + appel fonction afficher_laby
	contenu = JSON.stringify(donnees);
	fs.writeFileSync("labyrinthe2.json",contenu, "utf-8");
	
	marqueurs  = afficher_laby2();

	if (donnees.perso.y === 0 && donnees.perso.x === 0) {
		page = fs.readFileSync('modele_fini_jouer.html', 'utf-8');
	} else {
		page = fs.readFileSync("./Labyrinthe2/modele_laby2.html", "utf-8");
	}
    page = page.supplant(marqueurs);

    res.writeHead(200, { "content-type": "text/html" });
    res.write(page);
    res.end();

};
module.exports = req_deplacement2;
