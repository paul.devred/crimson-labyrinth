'use strict'

require('remedial')
const fs = require("fs");
const afficher_laby = require("./fct_afficher_laby2");
const ragekit = require("./fct_ragekit2");

const req_laby2 = function(req, res, query) {
	let contenu;
	let marqueurs;
	let page;
	let donnees;

	contenu = fs.readFileSync("labyrinthe2.json", "utf-8");
	donnees = JSON.parse(contenu);
	donnees = ragekit(donnees);

	contenu = JSON.stringify(donnees);
	fs.writeFileSync("labyrinthe2.json", contenu, "utf-8");

	marqueurs = afficher_laby();
	
	page = fs.readFileSync("./Labyrinthe2/modele_laby2.html", "utf-8");
	page = page.supplant(marqueurs);

	res.writeHead(200, { "content-type": "text/html" });
	res.write(page);
	res.end();
};

module.exports = req_laby2;
